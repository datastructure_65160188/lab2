/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Arrays;

/**
 *
 * @author black
 */
public class Lab2_1 {

    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};

        double[] values = new double[4];

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }

        System.out.println();
        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }

        values[0] = 5.3;
        values[1] = 8.7;
        values[2] = 3.2;
        values[3] = 2.7;

        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }
        System.out.printf("\nThe sum of all elements in the numbers array is " + sum);

        double max = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            } else {
                continue;
            }
        }
        System.out.printf("\nThe maximum value in the values array is " + max);

        String[] reversedNames = new String[4];

        for (int i = 0; i < names.length; i++) {
            String j = names[i];
            reversedNames[i] = names[names.length - i - 1];
            reversedNames[names.length - i - 1] = j;
        }
        System.out.println();

        for (int i = 0; i < reversedNames.length; i++) {
            System.out.print(reversedNames[i] + " ");
        }
        System.out.println();

        Arrays.sort(numbers);
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
    }
}
